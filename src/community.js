export default function updateCommunityCards(data) {
    const communitySection = document.querySelector('.app-section--community');
    const cards = communitySection.querySelectorAll('.app-section__card');

    for (let i = 0; i < data.length; i++) {
        const card = cards[i];
        const cardData = data[i];

        const cardImg = card.querySelector('.card-img');
        const cardName = card.querySelector('.card-name');
        const cardPosition = card.querySelector('.card-position');

        cardImg.src = cardData.avatar;
        cardImg.alt = `${cardData.firstName} ${cardData.lastName}`;
        cardName.textContent = `${cardData.firstName} ${cardData.lastName}`;
        cardPosition.textContent = cardData.position;
    }
}
