import addJoinOurProgramSection from './join-us-section.js';
import updateCommunityCards from './community.js';
import './styles/style.css';

document.addEventListener('DOMContentLoaded', () => {
    addJoinOurProgramSection('standard');

    const fetchStartTime = performance.now();

    // Make a GET request using fetch and call the function
    fetch('/community')
        .then((response) => {
            if (!response.ok) {
                throw new Error(`Request failed with status: ${response.status}`);
            }
            return response.json();
        })
        .then((data) => {
            const fetchEndTime = performance.now();
            const fetchTime = fetchEndTime - fetchStartTime;
            console.log(`Fetch time: ${fetchTime} ms`);
            updateCommunityCards(data);
        })
        .catch((error) => {
            console.error('Request failed:', error);
        });

        window.addEventListener('load', () => {
            const loadEndTime = performance.now();
            const loadTime = loadEndTime - window.performance.timing.navigationStart;
            console.log(`Page load time: ${loadTime} ms`);
          });

          if (performance.memory) {
            const memoryUsage = performance.memory.usedJSHeapSize 
            console.log(`Memory usage: ${memoryUsage.toFixed(2)} MB`);
          }

          const metrics = {
            fetchTime,
            loadTime,
            memoryUsage
          };
          
          // Sending metrics to the server
          fetch('http://localhost:8080/analytics/performance', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(metrics)
          })
          .then(response => {
            if (response.ok) {
              console.log('Metrics sent successfully');
            } else {
              console.error('Failed to send metrics');
            }
          })
          .catch(error => {
            console.error('Error sending metrics:', error);
          });
          
});
